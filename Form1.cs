﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace File_search
{
    
    public partial class Form1 : Form
    {
        SearchParameters sp;
        CancellationTokenSource cts;
        CancellationToken token;
        string someFile;
        delegate void AddFile(TreeNode parent, string file);
        delegate void AddDirectory(TreeNode parent, string dir);
        AddDirectory ad;
        AddFile af;
        bool isContinue;
        public Form1()
        {
            InitializeComponent();
            isContinue = false;
            sp = SearchParameters.Deserialize(); //если нет файла?
            textBox1.Text = sp.searchDirectory;
            textBox2.Text = sp.filenameTemplate;
            textBox3.Text = sp.characterSet;
        }
        private async void Search_Click(object sender, EventArgs e)
        {
            sp = new SearchParameters()
            {
                searchDirectory = textBox1.Text,
                filenameTemplate = textBox2.Text,
                characterSet = textBox3.Text
            };
            SearchParameters.Serialize(sp);

            var root = new TreeNode() { Text = sp.searchDirectory, Tag = sp.searchDirectory };
            treeView1.Nodes.Clear();
            treeView1.Nodes.Add(root);

            cts = new CancellationTokenSource();
            token = cts.Token;
            af = new AddFile(addFile);
            ad = new AddDirectory(addDirectory);
            await Task.Run(() => Build(root, token));
            //Build(root, token);
        }
        private void Build(TreeNode parent, CancellationToken token)
        {
            var path = parent.Tag as string;
            //parent.Nodes.Clear();

            //create files
            foreach (var file in Directory.GetFiles(path))
            {
                if (token.IsCancellationRequested)
                {
                    //остановить таймер, обрабатываемый файл лейбл

                    someFile = Path.Combine(Path.GetFullPath(file), file);
                    
                    return;
                }
                if (Path.GetFileName(file).Contains(sp.filenameTemplate) && SearchParameters.checkFileContent(Path.GetFullPath(file), sp.characterSet))
                {
                    treeView1.Invoke(af, parent, file);
                }
            }

            //create dirs
            
            
            foreach (var dir in Directory.GetDirectories(path))
            {
                treeView1.Invoke(ad, parent, dir);
                Build(parent.LastNode, token);
            }
            
        }

        public void addFile(TreeNode parent, string file)
        {
            parent.Nodes.Add(new TreeNode(Path.GetFileName(file), 1, 1) { Tag = file });
        }
        public void addDirectory(TreeNode parent, string dir)
        {
            parent.Nodes.Add(new TreeNode(Path.GetFileName(dir), new[] { new TreeNode("...") }) { Tag = dir });
        }

        private void ChooseDirectory_Click(object sender, EventArgs e)
        {
            string folderName;
            using (folderBrowserDialog1)
            {
                if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
                {
                    folderName = folderBrowserDialog1.SelectedPath;
                    textBox1.Text = folderName;
                }
            }
        }

        private async void Continue_Click(object sender, EventArgs e)
        {
            

            cts = new CancellationTokenSource();
            token = cts.Token;
            //af = new AddFile(addFile);
            //ad = new AddDirectory(addDirectory);
            await Task.Run(() => Build2(treeView1.Nodes[0], token));
        }
        private void Build2(TreeNode parent, CancellationToken token)
        {
            var path = parent.Tag as string;
            //parent.Nodes.Clear();

            //create files
            foreach (var file in Directory.GetFiles(path))
            {
                if(Path.Combine(Path.GetFullPath(file), file) == someFile)
                {
                    isContinue = true;
                }
                if (isContinue)
                {
                    if (token.IsCancellationRequested)
                    {
                        //остановить таймер, обрабатываемый файл лейбл

                        someFile = Path.Combine(Path.GetFullPath(file), file);
                        isContinue = false;
                        return;
                    }
                    if (Path.GetFileName(file).Contains(sp.filenameTemplate) && SearchParameters.checkFileContent(Path.GetFullPath(file), sp.characterSet))
                    {
                        treeView1.Invoke(af, parent, file);
                    }
                }
            }

            //create dirs


            foreach (var dir in Directory.GetDirectories(path))
            {
                if (isContinue)
                {
                    treeView1.Invoke(ad, parent, dir);
                }
                Build2(parent.LastNode, token);          
            }

        }

        private void Stop_Click(object sender, EventArgs e)
        {
            if (cts != null)
                cts.Cancel();
        }
    }

}
