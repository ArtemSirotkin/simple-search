﻿using System;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;

namespace File_search
{
    [Serializable]
    public class SearchParameters
    {
        public string searchDirectory { get; set; }
        public string filenameTemplate { get; set; }
        public string characterSet { get; set; }

        public static void Serialize(SearchParameters sp)
        {
            FileStream stream = File.Create("searchParameters.dat");
            BinaryFormatter formatter = new BinaryFormatter();
            //Сериализация
            formatter.Serialize(stream, sp);
            stream.Close();



        }
        public static SearchParameters Deserialize()
        {
            FileStream stream = File.OpenRead("searchParameters.dat");
            BinaryFormatter formatter = new BinaryFormatter();
            SearchParameters sp = formatter.Deserialize(stream) as SearchParameters;
            stream.Close();
            return sp;
        }

        public static bool checkFileContent(string path, string row)
        {
            using (StreamReader sr = new StreamReader(path, System.Text.Encoding.Default))
            {
                string line = sr.ReadToEnd();
                bool satisfying = true;
                for (int i = 0; i < row.Length; i++)
                {
                    if (!line.Contains(row[i]))
                    {
                        satisfying = false;
                        break;
                    }
                }
                return satisfying;
            }
        }
    }

}